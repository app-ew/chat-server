const uuidv1 = require('uuid/v1');

module.exports = {
    async upload(file) {
        let {name} = file;
        name = uuidv1() + '.' + name.split('.').pop();

        return new Promise(((resolve, reject) => {
            file.mv('./files/' + name, function(err) {
                if (err)
                    reject(false);

                resolve(name)
            });

        }))
    }
}