var mysql = require('mysql');

let connection = null;

module.exports = {
    open(user = "root", password = "") {
        connection = mysql.createConnection({
            host: 'localhost',
            database: 'chat',
            user,
            password
        });
        connection.connect();
    },
    async getUser(Login = 'User1') {
        return new Promise((resolve, reject) => {
            //connection.connect();
            connection.query(`SELECT * from \`users\` where Login = ?`, Login, function (err, rows, fields) {
                if (err) {
                    reject(err);
                }
                //connection.end();
                resolve(rows[0]);
            })
        })
    },
    async getMessages(idChat = 1) {
        return new Promise((resolve, reject) => {
            //connection.connect();
            connection.query(`SELECT * from \`messages\` where idChat = ${Number(idChat)}`, function (err, rows, fields) {
                if (err) {
                    reject(err);
                }
                //connection.end();
                resolve(rows);
            })
        })
    },
    async getChats(idUser = 1) {
        return new Promise(resolve => {
            //connection.connect();
            connection.query(`SELECT * from \`chats\``, async function (err, rows, fields) {
                if (err) {
                    reject(err);
                }
                //connection.end();

                let chats = rows.filter(chat => {
                    let users = JSON.parse(chat.Users);
                    if (chat.idUser == idUser) return true;
                    if (users.includes(idUser)) return true;
                    return false;
                })

                for (key in chats) {
                    let users = JSON.parse(chats[key].Users);
                    if (users.length == 0) {
                        chats[key].Users = [];
                        continue;
                    }
                    chats[key].Users = await new Promise(resolve1 => {
                        connection.query(`SELECT id, Login from \`users\` where id in (${users.join(', ')})`, function (err, rows, fields) {
                            if (err) {
                                console.log(err);
                            }
                            resolve1(rows)
                        })
                    })
                    chats[key].LastMessage = await new Promise(resolve1 => {
                        connection.query(`SELECT * FROM messages WHERE idChat=? ORDER BY id DESC LIMIT 1`, chats[key].id, function (err, rows, fields) {
                            if (err) {
                                console.log(err);
                            }
                            resolve1(rows[0])
                        })
                    })
                }

                resolve(chats);
            });
        })
    },
    async createChat(chat, idUser) {
        return new Promise((resolve, reject) => {
            connection.query(`INSERT INTO \`chats\`(\`idUser\`, Name, \`Desc\`, Users) VALUES(?, ?, ?, ?)`, [idUser, chat.Name, chat.Desc, JSON.stringify(chat.Users.map(user => user.id) || [])], function (err, rows, fields) {
                if (err) {
                    reject(false)
                }
                resolve(rows.insertId)
            })
        });
    },
    async updateChat(chat) {
        connection.query(`UPDATE \`chats\` SET Name=?, \`Desc\`=?, Users=? where id = ? `, [chat.Name, chat.Desc, JSON.stringify(chat.Users.map(user => user.id)), chat.id], function (err, rows, fields) {
            if (err) {
                console.log(err);
            }
        });
    },
    async removeChat(chat) {
        return new Promise((resolve, reject) => {
            connection.query(`DELETE FROM \`chats\` where id = ? `, chat.id, function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    reject(false);
                }

                connection.query(`DELETE FROM \`messages\` where idChat = ? `, chat.id, function (err, rows, fields) {
                    if (err) {
                        console.log(err);
                        reject(false);
                    }
                    resolve(true)
                });
            });
        });
    },
    async newMessage(message) {
        return new Promise((resolve, reject) => {
            //connection.connect();
            connection.query(`INSERT INTO \`messages\` SET ?`, message, function (err, rows, fields) {
                if (err) {
                    reject(err);
                }
                //connection.end();
                resolve(rows);
            });
        })
    },
    async newUser(user) {
        return new Promise((resolve, reject) => {
            connection.query(`INSERT INTO \`users\` SET ?`, user, function (err, rows, fields) {
                if (err) {
                    reject(false);
                }
                resolve(rows.insertId);
            });
        })
    },
    async updateUser(user) {
        return new Promise((resolve, reject) => {
            connection.query(`UPDATE \`users\` SET Login = ?, Email = ?, Pass = ? where id = ? `, [user.Login, user.Email, user.Pass, user.id], function (err, rows, fields) {
                if (err) {
                    reject(false);
                    console.log(err)
                }
                resolve(true);
            });
        })
    },
    async updateToken(user) {
        return new Promise((resolve, reject) => {
            connection.query(`UPDATE \`users\` SET Token = ? where id = ? `, [user.Token, user.id], function (err, rows, fields) {
                if (err) {
                    reject(false);
                }
                resolve(true);
            });
        })
    },
    async testToken(Token) {
        return new Promise((resolve, reject) => {
            if (!Token) {
                reject(false);
                return;
            }
            connection.query(`SELECT * from \`users\` where Token = ?`, Token, function (err, rows, fields) {
                if (err) {
                    reject(false);
                }
                resolve(rows[0]);
            });
        })

    },
    async hasUser({Login, Pass}) {
        return new Promise((resolve, reject) => {
            connection.query(`SELECT * from \`users\` where Login = ? and Pass = ? `, [Login, Pass], function (err, rows, fields) {
                if (err) {
                    reject(err);
                }
                console.log(rows)
                resolve(rows);
            });
        })

    },
    close() {
        connection.end();
    }
}