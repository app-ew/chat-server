-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 20 2019 г., 15:25
-- Версия сервера: 5.7.20
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `chat`
--

-- --------------------------------------------------------

--
-- Структура таблицы `chats`
--

CREATE TABLE `chats` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Desc` text NOT NULL,
  `Users` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `chats`
--

INSERT INTO `chats` (`id`, `idUser`, `Name`, `Desc`, `Users`) VALUES
(1, 1, 'Чат #1', '', '[1,2,3]'),
(2, 2, 'Чат #2', '', '[1,2]'),
(15, 2, 'Чат №3', '', '[2,1]');

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `idChat` int(11) NOT NULL,
  `Text` longtext NOT NULL,
  `Append` text NOT NULL,
  `DateAt` bigint(20) NOT NULL,
  `FromId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `idChat`, `Text`, `Append`, `DateAt`, `FromId`) VALUES
(4, 1, 'Сообщение 1\r\n', '', 1558168651014, 1),
(5, 1, 'Сообщение 1\r\n', '', 1558168651014, 2),
(6, 1, 'Сообщение 2', '', 1558168695574, 1),
(7, 1, 'Сообщение 2', '', 1558168695574, 2),
(8, 1, 'Сообщение 3', '', 1558170018277, 1),
(9, 1, 'Сообщение 3', '', 1558170020964, 2),
(19, 1, '11\n', '', 1558179365248, 1),
(20, 1, '22', '', 1558179548140, 2),
(21, 1, '22\n', '', 1558179658841, 2),
(22, 1, '33', '', 1558179702717, 3),
(49, 1, '', '3b5a12a0-7a4d-11e9-bdd8-bffe216d3da8.jpg', 1558280775307, 1),
(50, 2, '', '41e778b0-7a4d-11e9-bdd8-bffe216d3da8.jpg', 1558280785682, 1),
(51, 1, '111', '', 1558282253214, 1),
(52, 1, '', 'b0880f70-7a50-11e9-bdd8-bffe216d3da8.jpg', 1558282260230, 1),
(53, 2, '11', '', 1558282266014, 1),
(54, 15, '', 'e29ea190-7a50-11e9-bdd8-bffe216d3da8.jpg', 1558282344355, 2),
(55, 1, 'Какая сейчас температура?', '', 1558353952652, 1),
(56, 1, 'Какая сейчас температура?', '', 1558354015459, 1),
(57, 1, 'Какая сейчас температура?', '', 1558354256359, 1),
(58, 1, 'Какая сейчас температура?', '', 1558354259023, 1),
(59, 1, 'Какая сейчас температура?', '', 1558354284739, 1),
(60, 1, 'Какая сейчас температура?', '', 1558354320551, 1),
(61, 1, 'Какая сейчас температура?', '', 1558354361900, 1),
(62, 1, '[object Promise]', '', 1558354361900, 17),
(63, 1, 'Какая сейчас температура?', '', 1558354458900, 1),
(64, 1, 'Сейчас 21 градусов', '', 1558354458900, 17),
(65, 1, 'Какая сейчас температура?', '', 1558354631862, 1),
(66, 1, 'Сейчас 21 градусов', '', 1558354631862, 17),
(67, 1, 'Какая сейчас температура?', '', 1558355074340, 1),
(68, 1, 'Сейчас 21 градусов', '', 1558355074340, 17),
(69, 1, 'Какая сейчас температура?', '', 1558355108152, 1),
(70, 1, 'Сейчас 21 градусов', '', 1558355108152, 17),
(71, 1, 'Какая сейчас температура?', '', 1558355116880, 1),
(72, 1, 'Сейчас 21 градусов', '', 1558355116880, 17);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `Login` varchar(20) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Pass` longtext NOT NULL,
  `Token` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `Login`, `Email`, `Pass`, `Token`) VALUES
(1, 'User1', 'UserLogin@mail.ru', '$2a$10$li3jRqg0VIUgjtgcoFbT9OuexfWeLyey.pv/U/L16VW9csn7uZ/7S', '0ff0c200-7afa-11e9-b941-9773fba9f74b'),
(2, 'User2', 'UserLogin@mail.ru', '$2a$10$yBs5JsTE7Z6EBhyH0KPsgeRT55jxXEj1Htz41LlIdZP8Y.u5dKaqW', NULL),
(3, 'User3', 'UserLogin@mail.ru', '$2a$10$Cszs5.8flUMKpWuFRQbPbegKePY5NmEq9zpTkNYjgFDpyk3UjegO6', NULL),
(12, 'UserLogin', 'UserLogin@mail.ru', '$2a$10$cXBGQ8GDggW.1geoejsm8e4AS2NTEPw1P.QUK9F.WmV1GoDXCYcpC', 'bcef6060-795b-11e9-9972-03db1956f1f6'),
(14, '123', '123', '$2a$10$yZT9fL.hxlMAUNDH7ArFkO/tQrIQRpd1I/m2WPDBDbsIyKK27wn9C', '22e6c900-7a3b-11e9-a3cc-47a3c3d74c65'),
(15, 'asdf', 'asdf', '$2a$10$2dS1Vxfpg/jBBom173VD9.ag7pGktWtMEVFDHSWSbZXQqo6GZbP8i', '60830850-7a3b-11e9-a3cc-47a3c3d74c65'),
(16, 'sasdf', 'sasdf', '$2a$10$sl4DYu7wIJnw.T6lQa/WL.hbInT5I9lS6uKqDspqrz/dbvV3JrX5y', '69374600-7a3b-11e9-a3cc-47a3c3d74c65'),
(17, 'Bot', 'UserLogin@mail.ru', '$2a$10$FBme6u4gWjL7vrPhSMLMfOsn2onZWHPrIf8MxXSPCdTB00rzldMV6', NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
