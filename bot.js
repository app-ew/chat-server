let weather = require('./weather')

module.exports = {
    test(chat) {
        if (chat.Users.find(user => user.Login == 'Bot')) return true;
        return false;
    },
    getKeyBoard(chat) {
        return ['Какая сейчас температура?']
    },
    async testMessage(message) {
        return new Promise(async resolve => {
            if (message.Text == 'Какая сейчас температура?') {
                resolve(`Сейчас ${await weather()} градусов`)
            } else resolve(false)
        })
    }
}