var db = require('./db');
const uuidv1 = require('uuid/v1');
const bcrypt = require('bcrypt-nodejs');

module.exports = {
    async newUser({Login, Pass, Email}) {
        Pass = bcrypt.hashSync(Pass);
        let Token = uuidv1();
        let id = await db.newUser({Login, Email, Pass, Token})
        return {id, Login, Email, Token}
    },
    async Auth({Login, Pass}) {
        let user = await db.getUser(Login);

        return new Promise((resolve, reject) => {
            bcrypt.compare(Pass, user.Pass, async function (err, res) {
                if (err || res == false) { reject(false); return; }

                let Token = uuidv1();
                await db.updateToken({id: user.id, Token}).catch(e => console.log(e));
                resolve({...await db.getUser(Login), Pass: null});
            })
        })

    },
    async LogOut({Token}) {
        let user = await db.testToken(Token)
        if (user)
            db.updateToken({id: user.id, Token: null})
    },
    async Update(User = {id, Login, Email}, Token) {
        await db.testToken(Token).then(async function (user) {
            if (user.id == User.id)
                await db.updateUser({...user, ...User})
        })
    },
    async UpdatePass(Pass, Token) {
        await db.testToken(Token).then(async function (user) {
            Pass = bcrypt.hashSync(Pass);
            await db.updateUser({...user, Pass})
        })

    }
}