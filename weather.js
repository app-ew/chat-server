const request = require('request')

let weather_token = 'ea18f89d62938e7790931fb36d63ab13';

module.exports = (city = 'Moscow') => {
    return new Promise(function (resolve) {
        request({
            url: `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${weather_token}`,
            json: true
        }, function (err, res, json) {
            if (err) {
                throw err;
            }
            resolve(Math.round(json.main.temp - 273, 15));
        });

    })
}