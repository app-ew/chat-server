var fs = require('fs');
var express = require('express');
var app = express();
let expressWs = require('express-ws')(app)
const fileUpload = require('express-fileupload');

let aWss = expressWs.getWss('/');

app.use(express.static('./html'));
app.use('/files', express.static('./files'));
app.use(fileUpload());

var db = require('./db');
var auth = require('./auth');
var upload = require('./upload');
let bot = require('./bot')

app.listen(1122, function () {
    console.log('server start http://localhost:1122');
});

expressWs.getWss().on('connection', function (ws) {
    console.log('connection ws open');
});

app.post('/upload', async function (req, res) {
    let payload = await upload.upload(req.files.file)
    res.send(payload)
});

let countQuerys = 0;

app.ws('/', async function (ws, req, next) {
    ws.on('message', async function incoming(data) {
        if (countQuerys == 0) db.open();
        countQuerys++;
        data = JSON.parse(data);
        console.log(`Request: ${data.action}`);

        if (data.action == 'new-user') {
            ws.send(JSON.stringify({action: 'auth', payload: await auth.newUser(data.payload)}))
        }

        if (data.action == 'new-message') {
            await db.testToken(data.Token).then(async function () {
                await db.newMessage(data.payload)

                let bot_answer = await bot.testMessage(data.payload);
                if (bot_answer) db.newMessage({...data.payload, Text: bot_answer, FromId: 17})

                aWss.clients.forEach(function (client) {
                    //console.log(client)
                    client.send(JSON.stringify(data));
                    if (bot_answer)
                        client.send(JSON.stringify({
                            ...data,
                            payload: {...data.payload, Text: bot_answer, FromId: 17}
                        }));
                });

            }).catch(e => {
                ws.send(JSON.stringify({action: error, payload: {text: 'Ошибка авторизации', route: '/'}}));
            })
        }

        if (data.action == 'load-messages') {
            await db.testToken(data.Token).then(async function () {
                let payload = await db.getMessages(data.payload);
                ws.send(JSON.stringify({action: "messages", payload}));
            }).catch(e => {
                ws.send(JSON.stringify({action: 'error', payload: {text: 'Ошибка авторизации', route: '/'}}));
            })
        }

        if (data.action == 'load-chats') {
            await db.testToken(data.Token).then(async function (user) {
                let payload = []
                let chats = await db.getChats(user.id);
                for (chat of chats) {
                    if (bot.test(chat)) chat.keyboard = bot.getKeyBoard(chat)
                    payload.push(chat)
                }
                ws.send(JSON.stringify({action: 'chats', payload}));
            }).catch(e => {
                ws.send(JSON.stringify({action: 'error', payload: {text: 'Ошибка авторизации', route: '/'}}));
            })
        }

        if (data.action == 'update-chat') {
            await db.testToken(data.Token).then(async function (user) {
                let payload = await db.updateChat(data.payload);
            }).catch(e => {
                ws.send(JSON.stringify({action: 'error', payload: {text: 'Ошибка авторизации', route: '/'}}));
            })
        }

        if (data.action == 'create-chat') {
            await db.testToken(data.Token).then(async function (user) {
                let id = await db.createChat(data.payload, user.id)
                if (id) {
                    ws.send(JSON.stringify({action: 'new-chat', payload: {...data.payload, id}}));
                } else {
                    ws.send(JSON.stringify({
                        action: 'error',
                        payload: {text: 'Ошибка созданения чата', route: '/chat'}
                    }));
                }
            }).catch(e => {
                ws.send(JSON.stringify({action: 'error', payload: {text: 'Ошибка авторизации', route: '/'}}));
            })
        }

        if (data.action == 'remove-chat') {
            await db.testToken(data.Token).then(async function (user) {
                if (await db.removeChat(data.payload, user.id)) {
                    ws.send(JSON.stringify({action: 'remove-chat', payload: {...data.payload.id}}));
                } else {
                    ws.send(JSON.stringify({
                        action: 'error',
                        payload: {text: 'Ошибка удаления чата', route: `/chat/${data.payload.id}`}
                    }));
                }
            }).catch(e => {
                ws.send(JSON.stringify({action: 'error', payload: {text: 'Ошибка авторизации', route: '/'}}));
            })
        }

        if (data.action == 'update-user') {
            await auth.Update(data.payload, data.Token)
        }

        if (data.action == 'update-user-pass') {
            await auth.UpdatePass(data.payload, data.Token)
        }

        if (data.action == 'log-out') {
            await auth.LogOut(data.payload)
        }

        if (data.action == 'log-in') {
            let payload = await auth.Auth(data.payload).catch(e => console.log(e))
            ws.send(JSON.stringify({action: 'auth', payload}))
        }

        if (data.action == 'find-user') {
            let payload = await db.getUser(data.payload);
            if (payload) {
                ws.send(JSON.stringify({action: 'has-user', payload: {id: payload.id, Login: payload.Login}}))
            } else {
                ws.send(JSON.stringify({action: 'has-user', payload: {id: null, Login: data.payload}}))
            }
        }

        if (data.action == 'chat-new-user') {
            const uuidv1 = require('uuid/v1');
            let Pass = uuidv1().substr(5, 5);
            let {id, Login} = await auth.newUser({Login: data.payload, Email: data.payload, Pass});
            ws.send(JSON.stringify({action: 'new-user-in-chat', payload: {id, Login}}))
        }

        countQuerys--;
        if (countQuerys == 0) db.close()

    });

    ws.on('close', function () {

    })

})
